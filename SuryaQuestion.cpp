#include<iostream>
using namespace std;
int greatestSum(int in[], int out[], int i, int j, int n){
    //Base Condition
    if(i>=n){
        int sum = 0;
        for(int k=0; k<j; k++){
            sum += out[k];
        }
        return sum;
    }
    //Recursive Call
    int inc = 0;
    int exc = 0;
    if(in[i]>=0){
        //Inclusion
        out[j] = in[i];
        inc = greatestSum(in, out, i+2, j+1, n);
    }
    //Exclusion
    exc = greatestSum(in, out, i+1, j, n);
    if(exc>inc){
        return exc;
    }    
    return inc;
}
int main(){
    int n;
    cin>>n;
    int in[1000];
    int out[1000]={};
    for(int i=0; i<n; i++){
        cin>>in[i];
    }
    cout<<greatestSum(in, out, 0, 0, n);
    return 0;
}