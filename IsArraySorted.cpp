#include<iostream>
using namespace std;
bool isSorted(int arr[], int n){
    if(n==1){
        return true;
    }
    if(arr[n-2]<=arr[n-1]){
        return isSorted(arr, n-1);
    }
    return false;
}
int main(){
    int n;
    cin>>n;
    int arr[1000];
    for(int i=0; i<n; i++){
        cin>>arr[i];
    }
    if(isSorted(arr, n)){
        cout<<"true"<<endl;
        return 0;
    }
    cout<<"false"<<endl;
    return 0;
}