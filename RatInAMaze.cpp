#include<iostream>
using namespace std;
bool MazeSolve(char in[][10], int out[][10], int m, int n, int x, int y){
    //Base Condition
    if(in[x][y]=='X'){
        return false;
    }
    if(x==n-1 && y==m-1){
        out[x][y] = 1;
        return true;
    }
    if(x>=n){
        return false;
    }
    if(y>=m){
        return false;
    }
    //Recursive Call
    if(MazeSolve(in, out, m, n, x, y+1)){
        out[x][y] = 1;
        return true;
    }
    else if(MazeSolve(in, out, m, n, x+1, y)){
        out[x][y] = 1;
        return true;
    }
    return false;
}
int main(){
    int n,m;
    cin>>n>>m;
    char arr[10][10];
    for(int i=0; i<n; i++){
        for(int j=0; j<m; j++){
            cin>>arr[i][j];
        }
    }
    int path[10][10] = {};
    if(MazeSolve(arr, path, m, n, 0, 0)){
        for(int i=0; i<n; i++){
            for(int j=0; j<m; j++){
                cout<<path[i][j]<<" ";
            }
            cout<<endl;
        }
    }
    else{
        cout<<"-1"<<endl;
    }
    return 0;
}