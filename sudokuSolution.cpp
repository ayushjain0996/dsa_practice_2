#include<iostream>
using namespace std;
bool isSafe(int arr[][9],int x,int y,int k){
    for(int i=x,j=y;i>=0;i--){
        if(arr[i][j]==k){
            return false;
        }
    }
    for(int i=x,j=y;j<9;j++){
        if(arr[i][j]==k){
            return false;
        }
    }
    for(int i=x,j=y;i<9;i++){
        if(arr[i][j]==k){
            return false;
        }
    }
    for(int i=x,j=y;j>=0;j--){
        if(arr[i][j]==k){
            return false;
        }
    }
    int startx = (x/3)*3;
    int starty = (y/3)*3;
    for(int i=0;i<3;i++){
        for(int j=0;j<3;j++){
            if(arr[i+startx][j+starty]==k){
                return false;
            }
        }
    }
    return true;
}
bool sudoko(int arr[][9],int n,int x,int y){
    if(x==n-1 && y==n){
        for(int i=0;i<n;i++){
            for(int j=0;j<n;j++){
                cout<<arr[i][j]<<" ";
            }
            cout<<endl;
        }
        return true;
    }
    if(y==n){
        y = 0;
        x = x+1;
    }

    if(arr[x][y]==0){
        for(int k=1;k<10;k++){
            if(isSafe(arr,x,y,k)){
                arr[x][y] = k;
                bool done = sudoko(arr,n,x,y+1);
                if(done){
                    return true;
                }
                arr[x][y] = 0;
            }
        }
    }else{
        return sudoko(arr,n,x,y+1);
    }
    return false;
}
int main(){
    int mat[9][9];
    int n;
    cin>>n;
    for(int i=0; i<n; i++){
        for(int j=0; j<n; j++){
            cin>>mat[i][j];
        }
    }
    cout<<endl;
    bool solve = sudoko(mat,9,0,0);
}
