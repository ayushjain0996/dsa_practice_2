#include<iostream>
using namespace std;
void pattern1(int n){
    //Base Condition
    if(n==0){
        return;
    }
    //Recursive Call
    pattern1(n-1);
    cout<<endl;
    for(int i=0; i<n; i++){
        cout<<"*\t";
    }
    return;
}
int main(){
    int n;
    cin>>n;
    pattern1(n);
    return 0;
}