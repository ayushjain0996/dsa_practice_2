#include<iostream>
using namespace std;
int subsequence(char input[], char output[], int i, int j){
    //Base Condition
    if(input[i]==0){
        output[j] = 0;
        cout<<output<<" ";
        return 1;
    }
    //Exclusion
    int exc = subsequence(input, output, i+1, j);
    //Inclusion
    //Normal
    output[j] = input[i];
    int incn = subsequence(input, output, i+1, j+1);
    //ASCII

    int x = (int)(input[i]);
    int r = 1;
    while(r<x){
        r = r * 10;
    }
    r = r/10;
    while(r>0){
        output[j] = (char)(x/r + '0');
        x = x % r;
        r = r/10;
        j = j + 1;
    }
    int inca = subsequence(input, output, i+1, j);
    return exc + incn + inca;
}
int main(){
    char input[1000];
    cin>>input;
    char output[2000];
    int result = subsequence(input, output, 0, 0);
    cout<<endl<<result<<endl;
    return 0;
}