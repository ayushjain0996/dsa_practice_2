#include<iostream>
using namespace std;
void merge(int arr[], int i, int n){
    int temp[1000];
    int x=0, y=i;
    int k=0;
    while (x<i && y<n)
    {
        if(arr[x]<arr[y]){
            temp[k++] = arr[x++];
        }
        else{
            temp[k++] = arr[y++];
        }
    }
    while(x<i){
        temp[k++] = arr[x++];
    }
    while(y<n){
        temp[k++] = arr[y++];
    }
    k--;
    while(k>=0){
        arr[k] = temp[k];
        k--;
    }
    return;
}
void sort(int arr[], int n){
    //Base Condition
    if(n<=1){
        return;
    }
    //Recursive Call
    if(n%2==0){
        sort(arr, n/2);
        sort(&arr[n/2], n/2);
        merge(arr, n/2, n);
    }
    else{
        sort(arr, n/2);
        sort(&arr[n/2], n/2+1);
        merge(arr, n/2, n);
    }
    return;
}
int main(){
    int n;
    int arr[1000];
    cin>>n;
    for(int i=0; i<n; i++){
        cin>>arr[i];
    }
    sort(arr, n);
    for(int i=0; i<n; i++){
        cout<<arr[i]<<" ";
    }
    return 0;
}