#include<iostream>
using namespace std;
void OddEven(int n){
    //Base Condition
    if(n==0){
        return;
    }
    //Recursive Call
    if(n%2==1){
        cout<<n<<endl;
        OddEven(n-1);
    }
    else{
        OddEven(n-1);
        cout<<n<<endl;
    }
    return;
}
int main(){
    int n;
    cin>>n;
    OddEven(n);
    return 0;
}