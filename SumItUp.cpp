#include<iostream>
using namespace std;
void Sort(int arr[], int n){
    for(int i=0; i<n-1; i++){
        int minval = arr[i];
        int minpos = i;
        for(int j=i+1; j<n; j++){
            if(minval>arr[j]){
                minval = arr[j];
                minpos = j;
            }
        }
        arr[minpos] = arr[i];
        arr[i] = minval;
    }
    return;
}
void sumItUp(int in[], int out[], int n, int num, int i, int j){
    //Base Condition
    if(i==n){
        int sum = 0;
        for(int k=0; k<j; k++){
            sum += out[k];
        }
        if(sum==num){
            for(int k=0; k<j; k++){
                cout<<out[k]<<" ";
            }
            cout<<endl;
        }
        return;
    }
    //Recursive Call
    //Inclusion
    out[j] = in[i];
    sumItUp(in, out, n, num, i+1, j+1);
    //Exclusion
    sumItUp(in, out, n, num, i+1, j);
    return;
}
int  main(){
    int n;
    cin>>n;
    int in[1000];
    int out[1000];
    for(int i=0; i<n; i++){
        cin>>in[i];
    }
    Sort(in, n);
    int sum;
    cin>>sum;
    sumItUp(in, out, n, sum, 0, 0);
    return 0;
}