#include <bits/stdc++.h>
using namespace std;


bool checker(int row, int col, int n, int m)
{
    return (row>=0 && row<n && col>=0 && col<m);
}

void func(int n, int m, vector< vector<char> > a)
{
    vector< vector<bool> > vis (n,vector<bool>(m));
    vector< pair<int,int> > directions = {{1,0},{0,1},{-1,0},{0,-1}};
    queue< pair<int,int> > s;
    s.push({0,0});
    vis[0][0]=true;
    vector< vector < pair<int,int> > > ans(n,vector< pair<int,int> >(m,{0,0}));
    while(!s.empty())
    {
        pair<int,int> p = s.front();
        s.pop();
        for(pair<int,int> dir:directions)
        {
            int new_row = p.first + dir.first;
            int new_col = p.second + dir.second;
            if(checker(new_row,new_col,n,m)&& !vis[new_row][new_col] && a[new_row][new_col]=='O')
            {
                ans[new_row][new_col].first=p.first;
                ans[new_row][new_col].second=p.second;
                // cout<<ans[new_row][new_col].first<<endl;
                s.push({new_row,new_col});
                vis[new_row][new_col]=true;
            }
        }
    }
    if(vis[n-1][m-1]==true)
    {
        // cout<<"visited"<<endl;
        vector< vector<int> > ans2 (n, vector<int> (m,0));
        // cout<<"a";
        ans2[0][0]=1;
        // cout<<"a";
        ans2[n-1][m-1] = 1;
        
        int l = ans[n-1][m-1].first;
        int k = ans[n-1][m-1].second;

        // cout<<l<<m;
        int temp;
        while (l!=0&&k!=0)
        {
            ans2[l][k]=1;
            temp = l;
            l = ans[l][k].first;
            k = ans[temp][k].second;
        }
        for(int i=0;i<n;i++)
        {
            // cout<<"a";
            for(int j=0;j<m;j++)
            {
                cout<<ans2[i][j]<<" ";
            }
            cout<<endl;
        }
    }
    else
    {
        cout<<"NO PATH FOUND";
    }
    
}

int main()
{
    int m,n;
    cin>>n>>m;
    vector< vector<char> > a;
    vector<char> temp;
    // cout<<"a";
    for(int i=0;i<n;i++)
    {
        // cout<<"aa";
        temp.clear();
        for(int j=0;j<m;j++)
        {
            char c;
            cin>>c;
            temp.push_back(c);
        }
        a.push_back(temp);
    }
    // cout<<"a";
    func(n,m,a);
    return 0;
}