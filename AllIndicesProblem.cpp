#include<iostream>
using namespace std;
void allIndices(int arr[], int start, int end, int num){
    if(start==end){
        return;
    }
    if(arr[start]==num){
        cout<<start<<" ";
    }
    allIndices(arr, start + 1, end, num);
    return;
}
int main(){
    int n;
    cin>>n;
    int arr[1000];
    for(int i=0; i<n; i++){
        cin>>arr[i];
    }
    int num;
    cin>>num;
    allIndices(arr, 0, n, num);
    return 0;
}