#include<iostream>
using namespace std;
void input(int arr[], int n){
    if(n==0){
        return;
    }
    cin>>arr[0];
    input(&arr[1], n-1);
    return;
}
void sort(int arr[], int n){
    if(n==1 || n==0){
        return;
    }
    int minval = arr[0];
    int minpos = 0;
    for(int i=1; i<n; i++){
        if(arr[i]<minval){
            minpos = i;
            minval = arr[i];
        }
    }
    arr[minpos] = arr[0];
    arr[0] = minval;
    sort(&arr[1], n-1);
    return;
}
void output(int arr[], int n){
    if(n==0){
        return;
    }
    cout<<arr[0]<<endl;
    output(&arr[1], n-1);
}
int main(){
    int n;
    cin>>n;
    int arr[1000];
    input(arr, n);
    sort(arr, n);
    output(arr, n);
    return 0;
}