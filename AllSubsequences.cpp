#include<iostream>
using namespace std;
void subsequences(char in[], char out[], int i, int j){
    // Base Condition
    if(in[i]=='\0'){
        out[j]='\0';
        cout<<out<<endl;
        int pos = 0;
        while(out[pos]!=0){
            pos++;
        }
        return;
    }
    subsequences(in, out, i+1, j);
    out[j]=in[i];
    subsequences(in, out, i+1, j+1);
    
    return;
}
int main(){
    char str[100];
    cin>>str;
    char out[100];
    subsequences(str, out, 0, 0);
    return 0;
}
