#include<iostream>
using namespace std;
int ways(int arr[], int n, int weight){
    if(weight==0){
        return 1;
    }
    if(weight<0){
        return 0;
    }
    int sum = 0;
    for(int i=0; i<n; i++){
        sum += ways(arr, i+1, weight - arr[i]);
    }
    return sum;
}
int main(){
    int n;
    cin>>n;
    int arr[1000];
    for(int i=0; i<n; i++){
        cin>>arr[i];
    }
    int weight;
    cin>>weight;
    cout<<ways(arr, n, weight);
    return 0;
}