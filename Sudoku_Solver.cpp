#include<iostream>
using namespace std;
bool isSafe(int arr[][9], int x, int y, int k){
    //n
    for(int i=x,j=y; i>=0; i--){
        if(arr[i][j]==k){
            return false;
        }
    }
    //e
    for(int i=x,j=y; j<9; j++){
        if(arr[i][j]==k){
            return false;
        }
    }
    //w
    for(int i=x,j=y; j>=0; j--){
        if(arr[i][j]==k){
            return false;
        }
    }
    //s
    for(int i=x,j=y; i<9; i++){
        if(arr[i][j]==k){
            return false;
        }
    }
    //square
    int startx = (x/3) * 3;
    int starty = (y/3) * 3;
    for(int i=0;i<3; i++){
        for(int j=0; j<3; j++){
            if(arr[i+startx][j+starty]==k){
                return false;
            }
        }
    }
    return true;
}
bool sudokuSolve(int arr[][9], int n, int x, int y){
    if(x==n-1 && y==n){
        for(int i=0; i<n; i++){
            for(int j=0; j<n; j++){
                cout<<arr[i][j]<<" ";
            }
            cout<<endl;
        }
        return true;
    }
    if(y==n){
        y=0;
        x=x+1;
    }
    if(arr[x][y]==0){
        for(int k=1; k<10; k++){
            if(isSafe(arr,x,y,k)){
                arr[x][y] = k;
                bool done = sudokuSolve(arr, n, x, y+1);
                if(done){
                    return true;
                }
                arr[x][y] = 0;
            }
        }
    }
    else
    {
        return sudokuSolve(arr,n,x,y+1);
    }
    return false;
}
int main(){
    int n;
    cin>>n;
    int sudoku[9][9];
    for(int i=0; i<n; i++){
        for(int j=0; j<n; j++){
            cin>>sudoku[i][j];
        }
    }
    bool solve = sudokuSolve(sudoku, n, 0, 0);

    return 0;
}