#include<iostream>
using namespace std;
void subset(int in[], int out[], int n, int sum, int i, int j){
    //Base Condition
    if(i==n){
        int add = 0;
        for(int k=0; k<j; k++){
            add += out[k];
        }
        if(add==sum){
            for(int k=j-1; k>=0; k--){
                cout<<out[k]<<" ";
            }
            cout<<endl;
        }
        return;
    }
    //Recursive Call
    //Exclusion
    subset(in, out, n, sum, i+1, j);
    //Inclusion
    out[j] = in[i];
    subset(in, out, n, sum, i+1, j+1);
    return;
}
int main(){
    int n;
    cin>>n;
    int in[20];
    for(int i=0; i<n; i++){
        cin>>in[i];
    }
    int out[20];
    int sum;
    cin>>sum;
    subset(in, out, n, sum, 0, 0);
    return 0;
}