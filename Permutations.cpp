#include<iostream>
using namespace std;
void swap(char& a, char& b){
    char temp = a;
    a = b;
    b = temp;
    return;
}
void permutations(char str[], int i){
    if(str[i]==0){
        cout<<str<<endl;
        return;
    }
    for(int j=i; str[j]!=0; j++){
        swap(str[i], str[j]);
        permutations(str, i+1);
        swap(str[i], str[j]);
    }
    return;
}

int main(){
    char str[10];
    cin.getline(str, 10);
    permutations(str, 0);
    return 0;
}