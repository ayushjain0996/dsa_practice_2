#include<iostream>
using namespace std;
int countHi(char str[]){
    //Base Condition
    if(str[0]==0){
        return 0;
    }
    //Recursive condition
    if(str[0]=='h' && str[1]=='i' && str[2]!='t'){
        return 1 + countHi(&str[2]);
    }
    else if(str[0]=='h' && str[1]=='i' && str[2]=='t'){
        return countHi(&str[3]);
    }
    else{
        return countHi(&str[1]);
    }
}
void removeHi(char str[]){
    //Base Condition
    if(str[0]==0){
        cout<<endl;
        return;
    }
    //Recursive Call
    if(str[0]=='h' && str[1]=='i' && str[2]!='t'){
        removeHi(&str[2]);
        return;
    }
    else if(str[0]=='h' && str[1]=='i' && str[0]=='t'){
        cout<<"hit";
        removeHi(&str[3]);
        return;
    }
    else{
        cout<<str[0];
        removeHi(&str[1]);
        return;
    }
    return;
}
void replaceHi(char str[]){
    //Base Condition
    if(str[0]==0){
        cout<<endl;
        return;
    }
    //Recursive Call
    if(str[0]=='h' && str[1]=='i' && str[2]!='t'){
        cout<<"bye";
        replaceHi(&str[2]);
        return;
    }
    else if(str[0]=='h' && str[1]=='i' && str[0]=='t'){
        cout<<"hit";
        replaceHi(&str[3]);
        return;
    }
    else{
        cout<<str[0];
        replaceHi(&str[1]);
        return;
    }
    return;
}
int main(){
    char str[1000]={};
    cin.getline(str, 1000);
    cout<<countHi(str)<<endl;
    removeHi(str);
    replaceHi(str);
    return 0;
}