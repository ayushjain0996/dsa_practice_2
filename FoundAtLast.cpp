#include<iostream>
using namespace std;
int lastIndex(int arr[], int n, int num){
    if(n==0){
        return -1;
    }
    if(arr[n-1]==num){
        return n-1;
    }
    return lastIndex(arr, n-1, num);
}
int main(){
    int n;
    cin>>n;
    int arr[1000];
    for(int i=0; i<n; i++){
        cin>>arr[i];
    }
    int num;
    cin>>num;
    cout<<lastIndex(arr, n, num)<<endl;
    return 0;
}