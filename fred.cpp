#include<iostream>
using namespace std;
int fred(int n){
    if(n==1){
        return 0;
    }
    int one = fred(n-1);
    int two = n-1;
    int three = n-1;
    if(n%3==0){
        three = fred(n/3);
    }
    if(n%2==0){
        two = fred(n/2);
    }
    if((one<two) && (one<three)){
        return 1 + one;
    }
    else if(two < three){
        return 1 + two;
    }
    else{
        return 1 + three;
    }
}
int main(){
    int T;
    cin>>T;
    for(int i=0; i<T; i++){
        int n;
        cin>>n;
        cout<<fred(n)<<endl;
    }
    return 0;
}