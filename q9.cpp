// This problem was asked by Airbnb.

// Given a list of integers, write a function that returns the largest sum of non-adjacent numbers. Numbers can be 0 or negative.

// For example, [2, 4, 6, 2, 5] should return 13, since we pick 2, 6, and 5. [5, 1, 1, 5] should return 10, since we pick 5 and 5.

// Follow-up: Can you do this in O(N) time and constant space?

#include<bits/stdc++.h>
using namespace std;

int func(int n)
{

    vector <int> a(n,0);
    for (int i = 0; i < n; i++)
    {
        cin>>a[i];
    }
    if(n==1)
    {
        return a[0];
    }
    int sum1=a[0],sum2=a[1];
    if(a[0]<0)
    {
        sum1=0;
    }
    if(a[1]<0)
    {
        sum2=0;
    }
    for (int i = 2; i < n; i++)
    {
        if(i==n-1)
        {
            if(a[i]>0)
                sum1+=a[i];
            break;
        }
        // cout<<"i "<<i<<"\n";
        // cout<<"a[i] "<<a[i]<<"\n";
        if(a[i]>a[i+1])
        {
            if(a[i]>0)
                sum1+=a[i];
            i++;
        }
        else
        {
            if(a[i+1]>0)
                sum1+=a[i+1];
            i+=2;
        }
    }
    for (int i = 3; i < n-1; i++)
    {
        if(i==n-1)
        {
            if(a[i]>0)
                sum2=a[i];
            break;
        }
        if(a[i]>a[i+1])
        {
            if(a[i]>0)
                sum2+=a[i];
            i++;
        }
        else
        {
            if(a[i+1]>0)
                sum2+=a[i+1];
            i+=2;
        }
    }
    return max(sum1,sum2); 
}

int main()
{
    int t;
    cin>>t;
    while (t--)
    {
        int n;
        cin>>n;
        cout<<func(n)<<"\n";
    }
    return 0;
}