#include<iostream>
using namespace std;
bool MazeSolve(char in[][10], int out[][10], int m, int n, int x, int y, int D){
    //Base Condition
    if(x==n-1 && y==m-1){
        out[x][y] = 1;
        return true;
    }
    if(x>=n || y>=m || x<0 || y<0){
        return false;
    }
    if(in[x][y]=='X'){
        return false;
    }
    if(out[x][y]==1){
        return false;
    }
    //Recursive Call
    if(D!=3 && MazeSolve(in, out, m, n, x, y+1, 1)){
        out[x][y] = 1;
        return true;
    }
    else if(D!=4 && MazeSolve(in, out, m, n, x+1, y, 2)){
        out[x][y] = 1;
        return true;
    }
    else if(D!=1 && MazeSolve(in, out, m, n, x, y-1, 3)){
        out[x][y] = 1;
        return true;
    }
    else if(D!=2 && MazeSolve(in, out, m, n, x-1, y, 4)){
        out[x][y] = 1;
        return true;
    }
    return false;
}
int main(){
    int n,m;
    cin>>n>>m;
    char arr[10][10];
    for(int i=0; i<n; i++){
        for(int j=0; j<m; j++){
            cin>>arr[i][j];
        }
    }
    int path[10][10] = {};
    if(MazeSolve(arr, path, m, n, 0, 0, 1)){
        for(int i=0; i<n; i++){
            for(int j=0; j<m; j++){
                cout<<path[i][j]<<" ";
            }
            cout<<endl;
        }
    }
    else{
        cout<<"NO PATH FOUND"<<endl;
    }
    return 0;
}