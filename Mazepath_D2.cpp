#include<iostream>
using namespace std;
int mazepath(int a, int b, int n, char path[], int i){
    if(a==0 && b==0){
        path[i] = 0;
        cout<<path<<" ";
        return 1;
    }
    if(a==0){
        path[i] = 'H';
        return mazepath(0,b-1,n,path,i+1);
    }
    if(b==0){
        path[i] = 'V';
        return mazepath(a-1,0,n,path,i+1);
    }
    path[i] = 'V';
    int v = mazepath(a-1,b,n,path,i+1);
    path[i] = 'H';
    int h = mazepath(a,b-1,n,path,i+1);
    int d = 0;
    if((a==b) || (a+b==n-1)){
        path[i] = 'D';
        d = mazepath(a-1,b-1,n,path,i+1);
    }
    return v + h + d;
}
int main(){
    int n;
    cin>>n;
    char path[1000] = {};
    int x = mazepath(n-1,n-1,n,path,0);
    cout<<endl<<x;
    return 0;
}