#include<iostream>
using namespace std;
void pattern(int n, int i){
    //Base Condition
    if(n==0){
        return;
    }
    if(i==n){
        cout<<endl;
        pattern(n-1, 0);
        return;
    }
    //Recursive Call
    cout<<"*\t";
    pattern(n, i+1);
    return;
}
int main(){
    int n;
    cin>>n;
    pattern(n, 0);
    return 0;
}