#include<iostream>
using namespace std;
int reverse(int x){
    if(x/10==0){
        return x;
    }
    int r = 1;
    while(r<x){
        r *= 10;
    }
    r = r/10;
    return (x%10)*r + reverse(x/10);
}
int main(){
    int t;
    cin>>t;
    while(t--){
        int a, b;
        cin>>a>>b;
        int sum = reverse(a) + reverse(b);
        cout<<reverse(sum)<<endl;
    }
    return 0;
}