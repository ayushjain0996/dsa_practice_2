#include<iostream>
using namespace std;

bool isSafe(int arr[][11], int n, int x, int y){
    //n
    int j=y, i=x;
    while(i>=0){
        if(arr[i][j]==1){
            return false;
        }
        i--;
    }
    //ne
    j=y;
    i=x;
    while(i>=0 && j<n){
        if(arr[i][j]==1){
            return false;
        }
        i--;
        j++;
    }
    //e
    j=y;
    i=x;
    while(j<n){
        if(arr[i][j]==1){
            return false;
        }
        j++;
    }
    //se
    j=y;
    i=x;
    while(i<n && j<n){
        if(arr[i][j]==1){
            return false;
        }
        i++;
        j++;
    }
    //s
    j=y;
    i=x;
    while(i<n){
        if(arr[i][j]==1){
            return false;
        }
        i++;
    }
    //sw
    j=y;
    i=x;
    while(i<n && j>=0){
        if(arr[i][j]==1){
            return false;
        }
        i++;
        j--;
    }
    //w
    j=y;
    i=x;
    while(j>=0){
        if(arr[i][j]==1){
            return false;
        }
        j--;
    }
    //nw
    j=y;
    i=x;
    while(i>=0 && j>=0){
        if(arr[i][j]==1){
            return false;
        }
        i--;
        j--;
    }
    return true;
}
int nqueens(int arr[][11], int n, int x){
    if(x==n){
        return 1;
    }
    int sum = 0;
    for(int i=0; i<n; i++){
        if(isSafe(arr,n,i,x)){
            arr[i][x] = 1;
            sum += nqueens(arr,n,x+1);
            arr[i][x] = 0;
        }
    }
    return sum;
}
int main(){
    int n;
    cin>>n;
    int arr[11][11];
    for(int i=0; i<n; i++){
        for(int j=0; j<n; j++){
            arr[i][j] = 0;
        }
    }
    cout<<nqueens(arr,n,0)<<endl;
    return 0;
}