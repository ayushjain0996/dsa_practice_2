#include<iostream>
using namespace std;
void printRev(int n){
    if(n==0){
        return;
    }
    int x;
    cin>>x;
    printRev(n-1);
    cout<<x<<" ";
}
int main(){
    int n;
    cin>>n;
    printRev(n);
    return 0;
}