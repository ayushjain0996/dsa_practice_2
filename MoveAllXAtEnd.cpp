#include<iostream>
using namespace std;
void moveXtoEnd(char str[]){
    if(str[0]==0){
        return;
    }
    if(str[0]=='x'){
        int i=0;
        while(str[i+1]){
            str[i] = str[i+1];
            i++;
        }
        str[i] = 'x';
    }
    moveXtoEnd(&str[1]);
    return;
}
int main(){
    char str[1000];
    cin>>str;
    moveXtoEnd(str);
    cout<<str<<endl;
    return 0;
}