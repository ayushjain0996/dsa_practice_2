#include<iostream>
using namespace std;
#define N 100000
void merge(char A[], char B[]){
    char temp[2*N];
    int i=0,j=0,k=0;
    while(A[i]!=0 && B[j]!=0){
        if(A[i]<B[j]){
            temp[k++] = A[i++];
        }
        else{
            temp[k++] = B[j++];
        }
    }
    while(A[i]!=0){
        temp[k++] = A[i++];
    }
    while(B[j]!=0){
        temp[k++] = B[j++];
    }
    temp[k] = 0;
    cout<<temp<<endl;
}

int main(){
    int T;
    cin>>T;
    for(int t=1; t<=T; t++){
        char A[N], B[N];
        cin>>A;
        cin>>B;
        merge(A,B);
    }
    return 0;
}