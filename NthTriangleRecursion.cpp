#include<iostream>
using namespace std;

int triangle(int n){
    //base condition
    if(n==0){
        return 0;
    }
    //recursive call
    return n + triangle(n-1);
}

int main(){
    int n;
    cin>>n;
    cout<<triangle(n)<<endl;
    return 0;
}