#include<iostream>
using namespace std;
int count = 0;
void subsequences(char str[], char out[], int i, int j){
    if(str[i]==0){
        out[j]='\0';
        cout<<out<<" ";
        count++;
        return;
    }
    subsequences(str, out, i+1, j);
    out[j] = str[i];
    subsequences(str, out, i+1, j+1);
    return;
}
int main(){
    char str[100];
    cin>>str;
    char out[100];
    subsequences(str, out, 0, 0);
    cout<<endl<<count<<endl;
    return 0;
}