#include<iostream>
using namespace std;
char keypad[10][5] = { " ", ".+@$", "abc", "def", "ghi", "jkl" , "mno", "pqrs" , "tuv", "wxyz" };
void skeypad(char input[], char output[], int i){
    if(input[i]=='\0'){
        output[i]=0;
        cout<<output<<endl;
        return;
    }
    //Recursive Condition
    int digit = input[i] - '0';
    for(int k=0; keypad[digit][k]!=0; k++){
        output[i] = keypad[digit][k];
        skeypad(input, output, i+1);
    }
    return;
}
int main(){
    char input[1000], output[1000];
    cin>>input;
    skeypad(input, output,0);
    return 0;
}