#include<iostream>
using namespace std;
int mazepath(int a, int b, char path[], int i){
    if(a==1 && b==1){
        path[i] = 0;
        cout<<path<<" ";
        return 1;
    }
    if(a==1){
        path[i] = 'H';
        return mazepath(1,b-1,path,i+1);
    }
    if(b==1){
        path[i] = 'V';
        return mazepath(a-1,1,path,i+1);
    }
    path[i] = 'V';
    int v = mazepath(a-1,b,path, i+1);
    path[i] = 'H';
    int h = mazepath(a,b-1,path, i+1);
    return v+h;
}
int main(){
    int a,b;
    cin>>a>>b;
    char path[1000] = {};
    int x = mazepath(a,b,path,0);
    cout<<endl<<x<<endl;
    return 0;
}