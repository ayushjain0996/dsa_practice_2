#include<iostream>
using namespace std;
int str2int(char str[]){
    if(str[1]==0){
        return (int)(str[0] - '0');
    }
    int prev = str2int(&str[1]);
    int copy = prev;
    int r=1;
    while(copy>0){
        r = r*10;
        copy = copy/10;
    }
    int curr = (int)(str[0] - '0');
    int num = prev + curr*r;
    return num;
}
int main(){
    char str[1000];
    cin>>str;
    cout<<str2int(str)<<endl;
    return 0;
}