#include<iostream>
using namespace std;
void format(char str[]){
    //Base Condition
    if(str[0]==0){
        return;
    }
    //Recursive Call
    if(str[1]==str[0]){
        cout<<str[0]<<"*";
    }
    else{
        cout<<str[0];
    }
    format(&str[1]);
    return;
}
int main(){
    char str[10000];
    cin.getline(str, 10000);
    format(str);
    return 0;
}