#include<iostream>
using namespace std;
void input(int arr[], int n){
    for(int i=0; i<n; i++){
        cin>>arr[i];
    }
    return;
}
void sort(int arr[], int n){
    //Base Condition
    if(n==1){
        return;
    }
    //Recursive Call
    for(int i=0; i<n-1; i++){
        if(arr[i]>arr[i+1]){
            int temp = arr[i];
            arr[i] = arr[i+1];
            arr[i+1] = temp;
        }
    }
    sort(arr, n-1);
    return;
}
void output(int arr[], int n){
    for(int i=0; i<n; i++){
        cout<<arr[i]<<endl;
    }
}
int main(){
    int n;
    cin>>n;
    int arr[1000];
    input(arr, n);
    sort(arr, n);
    output(arr, n);
    return 0;
}