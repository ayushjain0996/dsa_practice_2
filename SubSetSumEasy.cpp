#include<iostream>
using namespace std;
bool subSetSum(int input[], int output[], int i, int j, int n){
    //Base Condition
    if(i==n && j==0){
        return false;
    }
    if(i==n){
        int sum = 0;
        for(int k=0; k<j; k++){
            sum = sum + output[k];
        }
        if(sum==0){
            return true;
        }
        else{
            return false;
        }
    }
    //Exclusion
    bool exc = subSetSum(input, output, i+1, j, n);
    //Inclusion
    output[j] = input[i];
    bool inc = subSetSum(input, output, i+1, j+1, n);
    return exc || inc;

}
int main(){
    int T;
    cin>>T;
    for(int t=0; t<T; t++){   
        int n;
        cin>>n;
        int input[1000];
        for(int i=0; i<n; i++){
            cin>>input[i];
        }
        int output[1000];
        if(subSetSum(input, output, 0, 0, n)){
            cout<<"Yes"<<endl;
        }
        else{
            cout<<"No"<<endl;
        }
    }
    return 0;
}