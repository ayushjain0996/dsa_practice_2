#include<iostream>
using namespace std;
bool isSafe(int arr[][10], int n, int x, int y){
    int i=x, j=y;
    if(i>=1 && j>=2){
        if(arr[i-1][j-2]){
            return false;
        }
    }
    if(i>=1 && j<n-2){
        if(arr[i-1][j+2]){
            return false;
        }
    }
    if(i<n-1 && j>=0){
        if(arr[i+1][j-2]){
            return false;
        }
    }
    if(i<n-1 && j<n-2){
        if(arr[i+1][j+2]){
            return false;
        }
    }
    if(i>=2 && j>=1){
        if(arr[i-2][j-1]){
            return false;
        }
    }
    if(i>=2 && j<n-1){
        if(arr[i-2][j+1]){
            return false;
        }
    }
    if(i<n-2 && j>=1){
        if(arr[i+2][j-1]){
            return false;
        }
    }
    if(i<n-2 && j<n-1){
        if(arr[i+2][j+1]){
            return false;
        }
    }
    return true;
}
int NKnights(int arr[][10], int n, int x){
    if(x==n){
        for(int i=0; i<n; i++){
            for(int j=0; j<n; j++){
                if(arr[i][j]){
                    cout<<"{"<<i<<"-"<<j<<"} ";
                }
            }
        }
        cout<<" ";
        return 1;
    }
    int sum = 0;
    for(int i=0; i<n; i++){
        if(isSafe(arr, n, x, i)){
            arr[x][i] = 1;
            sum += NKnights(arr, n, x+1);
            arr[x][i] = 0;
        }
    }
    return sum;
}
int main(){
    int n;
    cin>>n;
    int arr[10][10] = {};
    int result = NKnights(arr, n, 0);
    cout<<endl<<result<<endl;
    return 0;
}