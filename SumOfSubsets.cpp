#include<iostream>

using namespace std;

int countSubsets(int arr[], int n, int sum){
    if(sum==0){
        return 1;
    }
    if(sum<0){
        return 0;
    }
    if(n==0){
        return 0;
    }
    if(sum<arr[n-1]){
        return countSubsets(arr, n-1, sum);
    }
    int Inc = countSubsets(arr, n-1, sum - arr[n-1]);
    int nInc = countSubsets(arr, n-1, sum);
    return Inc + nInc;
}

int main(){
    int n;
    cin>>n;
    int arr[1000];
    for(int i=0; i<n; i++){
        cin>>arr[i];
    }
    int sum;
    cin>>sum;
    cout<<countSubsets(arr, n, sum)<<endl;
    return 0;
}