#include<iostream>
using namespace std;
void swap(char& a, char& b){
    char temp=a;
    a=b;
    b=temp;
    return;
}
int permutationsC(char arr[], int i){
    if(arr[i]==0){
        return 1;
    }
    int sum = 0;
    for(int j=i; arr[j]!=0; j++){
        swap(arr[j], arr[i]);
        sum += permutationsC(arr, i+1);
        swap(arr[j], arr[i]);
    }
    return sum;
}
void permutations(char arr[], int i){
    if(arr[i]==0){
        cout<<arr<<" ";
        return;
    }
    for(int j=i; arr[j]!=0; j++){
        swap(arr[j], arr[i]);
        permutations(arr, i+1);
        swap(arr[j], arr[i]);
    }
    return;
}
void sort(char arr[], int i){
    if(arr[i]==0){
        return;
    }
    char min=arr[i];
    int minIndex=i;
    for(int j=i+1; arr[j]!='\0'; j++){
        if(arr[j]<min){
            min=arr[j];
            minIndex=j;
        }
    }
    arr[minIndex]=arr[i];
    arr[i]=min;
    sort(arr, i+1);
}
int main(){
    char str[9];
    cin.getline(str, 9);
    sort(str,0);
    //cout<<str<<endl;
    cout<< permutationsC(str,0)<<endl;
    permutations(str, 0);
    cout<<endl;
    return 0;
}