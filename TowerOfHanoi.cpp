#include<iostream>
using namespace std;
void tohanoi(int n, char start, char end, char help){
    if(n==0){
        return;
    }
    tohanoi(n-1, start, help, end);
    cout<<"Moving ring "<<n<<" from "<<start<<" to "<<end<<endl;
    tohanoi(n-1, help, end, start);
    return;
}
int main(){
    int n;
    cin>>n;
    tohanoi(n, 'A', 'C', 'B');
    return 0;
}