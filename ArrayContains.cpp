#include<iostream>
using namespace std;
bool isPresent(int arr[], int n, int num){
    //Base Condition
    if(n==0){
        return false;
    }
    //Recursive Call
    if(arr[0]==num){
        return true;
    }
    else{
        return isPresent(&arr[1], n-1, num);
    }
}
int main(){
    int n;
    cin>>n;
    int arr[1000];
    for(int i=0; i<n; i++){
        cin>>arr[i];
    }
    int num;
    cin>>num;
    if(isPresent(arr, n, num)){
        cout<<"true";
    }
    else{
        cout<<"false";
    }
    cout<<endl;
    return 0;
}