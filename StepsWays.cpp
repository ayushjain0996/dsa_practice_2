#include<iostream>

using namespace std;

int num_ways(int steps, int arr[], int n){
    if(steps<0){
        return 0;
    }
    if(steps==0 || steps==1){
        return 1;
    }
    int to_return=0;
    for(int i=0; i<n; i++){
        to_return += num_ways(steps-arr[i], arr, n);
    }
    return to_return;
}

int main(){
    int n;

    return 0;
}