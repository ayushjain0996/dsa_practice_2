#include<iostream>
using namespace std;
int mazepath(int a, int b){
    if(a==1 && b==1){
        return 1;
    }
    if(a==1){
        return 1;
    }
    if(b==1){
        return 1;
    }
    int v = mazepath(a-1, b);
    int h = mazepath(a, b-1);
    int d = mazepath(a-1, b-1);
    return h + v + d; 
}
void pathTrace(int a, int b, char path[], int i){
    if(a==1 && b==1){
        path[i] = 0;
        cout<<path<<" ";
        return;
    }
    if(a==1){
        path[i] = 'H';
        pathTrace(1,b-1,path,i+1);
        return;
    }
    if(b==1){
        path[i] = 'V';
        pathTrace(a-1, 1, path, i+1);
        return;
    }
    path[i] = 'V';
    pathTrace(a-1,b,path,i+1);
    path[i] = 'H';
    pathTrace(a,b-1,path,i+1);
    path[i] = 'D';
    pathTrace(a-1,b-1,path,i+1);
    return;
}
int main(){
    int a, b;
    cin>>a>>b;
    char path[1000]={};
    pathTrace(a,b,path,0);
    cout<<endl<<mazepath(a,b)<<endl;
    return 0;
}